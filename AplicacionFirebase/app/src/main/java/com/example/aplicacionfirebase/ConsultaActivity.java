package com.example.aplicacionfirebase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ConsultaActivity extends AppCompatActivity
{


    private ListView lvItems;
    private Adaptador adaptador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consulta);
        ArrayList<Producto> productos = (ArrayList<Producto>) getIntent().getSerializableExtra("arrayProductos");

        lvItems = (ListView) findViewById(R.id.lvConsulta);
        adaptador = new Adaptador(this, productos);

        lvItems.setAdapter(adaptador);




/*
        ListView lv = findViewById(R.id.lvConsulta);
        ArrayList<Producto> productos = (ArrayList<Producto>) getIntent().getSerializableExtra("arrayProductos");


        ArrayList<String> texto = new ArrayList<>();

        for (Producto prods : productos) {


            texto.add(prods.getNombre() + " $" + prods.getValor() + " cantidad: " + prods.getCantidad());

        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, texto);

        lv.setAdapter(adapter);
*/
    }

}

