package com.example.aplicacionfirebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class InvitadoActivity extends AppCompatActivity {

    private EditText etCedula, etContraseña;
    CollectionReference productosref;
    FirebaseFirestore db = FirebaseFirestore.getInstance();


    ArrayList<Producto> productos = new ArrayList<Producto>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitado);



        etContraseña = findViewById(R.id.etContraseña);
        etCedula = findViewById(R.id.etCedula);
    }

    public void consultar(View view) {
        if (etCedula.getText().toString().isEmpty() || etContraseña.getText().toString().isEmpty()) {
            Toast.makeText(this, "Necesitas llenar todos los campos!", Toast.LENGTH_SHORT).show();
        } else {
            Integer menor = Integer.parseInt(etCedula.getText().toString());
            Integer mayor = Integer.parseInt(etContraseña.getText().toString());
            DocumentReference docRef = db.collection("usuarios").document(etCedula.getText().toString());
            docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            Toast.makeText(InvitadoActivity.this, "El producto se ha añadido! :)", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.d("E", "No such document");
                        }
                    } else {
                        Log.d("e", "get failed with ", task.getException());
                    }
                }
            });

        }

    }
}
