package com.example.aplicacionfirebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.annotations.Nullable;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

public class MainActivity extends AppCompatActivity {

    private Button btnInvitado, btnAdmin;
    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnInvitado = findViewById(R.id.btnInvitado);
        btnAdmin = findViewById(R.id.btnAdmin);


        db.collection("productos").addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w("", "listen:error", e);
                            return;
                        }

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            switch (dc.getType()) {
                                case ADDED:

                                    crearNotificacion(dc.getDocument().get("nombre") + " se ha agregado! \uD83D\uDE04", "con el precio de $"+ dc.getDocument().get("valor").toString());
                                    break;
                                case MODIFIED:

                                    crearNotificacion(dc.getDocument().get("nombre") + " se ha modificado! \uD83D\uDE04", "con el precio de $"+ dc.getDocument().get("valor").toString());
                                    break;
                                case REMOVED:

                                    crearNotificacion(dc.getDocument().get("nombre") + " se ha eliminado! \uD83D\uDE04", "con el precio de $"+ dc.getDocument().get("valor").toString());
                                    break;
                            }
                        }

                    }
                });


    }

    public void entrarInvitado(View view) {
        Intent ni = new Intent(MainActivity.this , AdminActivity.class);
        MainActivity.this.startActivity(ni);
    }

    public void entrarAdmin(View view) {
        Intent nii = new Intent(MainActivity.this,InvitadoActivity.class);
        MainActivity.this.startActivity(nii);

    }

    public void crearNotificacion(String titulo, String subtitulo){
        Intent it = new Intent(this, MainActivity.class);

        it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pi = PendingIntent.getActivity(this, 0, it, PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notMan = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Uri defaultsounduri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Notification.Builder notification = new Notification.Builder(this).setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle(titulo)
                .setContentText(subtitulo)
                .setAutoCancel(true)
                .setSound(defaultsounduri)
                .setContentIntent(pi);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            String channelId = getString(R.string.channelid);
            String channelName = getString(R.string.channelname);

            NotificationChannel channel = new NotificationChannel(channelId,channelName, notMan.IMPORTANCE_DEFAULT);
            channel.enableVibration(true);
            channel.setVibrationPattern(new long[]{100,200,200,50});
            if (notMan != null){
                notMan.createNotificationChannel(channel);
            }
            notification.setChannelId(channelId);
        }

        if (notMan != null){
            notMan.notify(732498, notification.build());
        }

    }
}
