package com.example.aplicacionfirebase;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class AdminActivity extends AppCompatActivity {
    private EditText etNombre, etValor, etCantidad;
    private Button btnIngresar;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
// ...

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        etNombre = findViewById(R.id.etNombre);
        etValor = findViewById(R.id.etCedula);
        etCantidad = findViewById(R.id.etContraseña);


    }

    public void IngresarProducto(View view) {
        if (etNombre.getText().toString().isEmpty() || etCantidad.getText().toString().isEmpty() || etValor.getText().toString().isEmpty()) {
            Toast.makeText(this, "Necesitas llenar todos los campos!", Toast.LENGTH_SHORT).show();
        } else {

            Map<String, Object> prod = new HashMap<>();
            prod.put("nombre", etNombre.getText().toString());
            prod.put("valor", Integer.parseInt(etValor.getText().toString()));
            prod.put("cantidad", Integer.parseInt(etCantidad.getText().toString()));

            db.collection("productos").document()
                    .set(prod)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast.makeText(AdminActivity.this, "El producto se ha añadido! :)", Toast.LENGTH_SHORT).show();

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(AdminActivity.this, "Hubo en error añadiendo el producto! :(", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }
}
