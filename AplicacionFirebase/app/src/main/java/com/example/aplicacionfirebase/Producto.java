package com.example.aplicacionfirebase;

import java.io.Serializable;

public class Producto implements Serializable {
    public String nombre;
    public Integer valor, cantidad;

    public Producto() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Producto(String nombre, Integer valor, Integer cantidad) {
        this.nombre = nombre;
        this.valor = valor;
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
}