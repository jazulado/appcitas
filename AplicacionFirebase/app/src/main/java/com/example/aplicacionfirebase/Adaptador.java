package com.example.aplicacionfirebase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.example.aplicacionfirebase.R;

import java.util.ArrayList;

public class Adaptador extends BaseAdapter {

    private Context contx;

    private ArrayList<Producto> prodarray;

    public Adaptador(Context contx, ArrayList<Producto> prodarray) {
        this.contx = contx;
        this.prodarray = prodarray;
    }

    @Override
    public int getCount() {
        return prodarray.size();
    }

    @Override
    public Object getItem(int position) {
        return prodarray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(contx).inflate(R.layout.item, null);

        Producto item = (Producto) getItem(position);

        TextView tvTitulo = (TextView) convertView.findViewById(R.id.titulo);
        TextView tvSubtitulo = (TextView) convertView.findViewById(R.id.subtitulo);
        TextView tvCantidad = (TextView) convertView.findViewById(R.id.cantidad);

        tvTitulo.setText(item.getNombre());
        tvSubtitulo.setText("Precio: $" + item.getValor());
        tvCantidad.setText("Cantidad: " + item.getCantidad());

        return convertView;
    }
}
