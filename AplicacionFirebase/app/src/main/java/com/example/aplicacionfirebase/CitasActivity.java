package com.example.aplicacionfirebase;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class CitasActivity extends AppCompatActivity {
    Button btnConsultar,btnAgregar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_citas);
        btnConsultar=findViewById(R.id.btnConsultar);
        btnAgregar=findViewById(R.id.btnAgregar);
    }

    public void consultarCita(View view) {
        Intent intent = new Intent (CitasActivity.this,ConsultaActivity.class);
        //intent.putExtra("arrayCitas", Citas);
        CitasActivity.this.startActivity(intent);
    }

    public void agregarcita(View view) {
        Intent intent = new Intent (CitasActivity.this,AgregarCitaActivity.class);
        CitasActivity.this.startActivity(intent);
    }
}
